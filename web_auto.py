import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class TestTestluntan():
    def setup_method(self, method):
        self.driver = webdriver.Chrome()
        self.vars = {}

    def teardown_method(self, method):
        self.driver.quit()

    def test_testluntan(self):
        self.driver.get("https://ceshiren.com/")
        self.driver.set_window_size(1057, 813)
        time.sleep(3)
        assert self.driver.title == "测试人社区 - 软件测试开发爱好者的交流社区，交流范围涵盖软件测试、自动化测试、UI测试、接口测试、性能测试、安全测试、测试开发、测试平台、开源测试、测试教程、测试面试题、appium、selenium、jmeter、jenkins"
        time.sleep(3)
        self.driver.find_element(By.CSS_SELECTOR, ".d-icon-search").click()
        time.sleep(3)
        self.driver.find_element(By.ID, "search-term").click()
        time.sleep(3)
        self.driver.find_element(By.ID, "search-term").send_keys("学习笔记")
        time.sleep(3)
        self.driver.find_element(By.CSS_SELECTOR, ".extra-hint").click()
        time.sleep(3)
        self.driver.find_element(By.CSS_SELECTOR, ".item:nth-child(2) .category-name").click()
        time.sleep(3)
        self.driver.find_element(By.LINK_TEXT, "【笔记】Pytest学习笔记 –待完善").click()
        time.sleep(3)
        self.driver.find_element(By.CSS_SELECTOR, "html").click()
        time.sleep(3)
