import pytest

def add(a, b):
    return a + b

@pytest.mark.hebeu
def test_add_int():
    print("开始计算")
    assert add(1, 5) == 6
    assert add(52, 71) == 123
    assert add(1, 22) == 23
    assert add(9, 23) == 32
    assert add(11, 14) == 25
    print("结束计算")

@pytest.mark.hebeu
def test_add_float():
    print("开始计算")
    assert add(0.05, 0.7) == pytest.approx(0.75, 0.01)
    assert add(5.3, -2.3) == pytest.approx(3.0, 0.01)
    assert add(-1.5, 22) == pytest.approx(20.5, 0.01)
    assert add(6.4, 3.6) == pytest.approx(10.0, 0.01)
    assert add(8.6, -10.5) == pytest.approx(-1.9, 0.01)
    print("结束计算")
