import allure
import requests

@allure.feature("冒烟测试")
class TestPett:
    @allure.story("宠物商店宠物信息接口测试post接口")
    def test_post(self):
        with allure.step("发出查询接口请求"):
            r = requests.post("https://petstore.swagger.io/v2/pet")
            with allure.step("获取查询接口响应"):
                print(r.json())
                with allure.step("查询接口断言"):
                    assert r.status_code == 415

@allure.story("宠物商店宠物信息接口测试get接口")
def get_test(self):
    with allure.step("发出查询接口请求"):
        r = requests.get("https://petstore.swagger.io/v2/pet/findByStatus?status=available")
        with allure.step("获取查询接口响应"):
            print(r.json())
            with allure.step("查询接口断言"):
                assert r.status_code == 200
